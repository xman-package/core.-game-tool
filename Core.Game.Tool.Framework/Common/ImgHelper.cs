﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Text;

namespace Core.GameTool.Framework.Common
{
    public class ImgHelper
    {
        public  static Image BtsToImage(byte[] bts)
        {
            using (MemoryStream ms = new MemoryStream(bts))
            {
                Image outputImg = Image.FromStream(ms);
                return outputImg;
            }
        }
    }
}
