﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.GameTool.Ocr
{
    public class OcrInfo
    {
        /// <summary>
        /// 
        /// </summary>
        public string Text { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Confidence { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public Text_Box_Position Text_Box_Position { get; set; }

        public Point CenterPosition => new((Text_Box_Position.LeftTop[0] + Text_Box_Position.RightBottom[0]) / 2, (Text_Box_Position.LeftTop[1] + Text_Box_Position.RightBottom[1]) / 2);

    }

    public class Text_Box_Position
    {
        /// <summary>
        /// 
        /// </summary>
        public List<int> LeftTop { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public List<int> RightTop { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public List<int> LeftBottom { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public List<int> RightBottom { get; set; }
    }
}


