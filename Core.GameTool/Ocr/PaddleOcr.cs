﻿using Core.GameTool.Framework.Common;
using Core.GameTool.Framework.Extensions;
using Core.GameTool.OpenCV;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Runtime.InteropServices;
using System.Text.Json;

namespace Core.GameTool.Ocr
{
    public class PaddleOcr
    {
        #region DllImport

        [DllImport("ocr.dll", EntryPoint = "Test", CallingConvention = CallingConvention.Cdecl)]
        private extern static void Test();

        [DllImport("ocr.dll", EntryPoint = "LoadConfig", CallingConvention = CallingConvention.Cdecl)]
        private extern static int LoadConfig(string path);

        [DllImport("ocr.dll", EntryPoint = "Ocr", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Ansi)]
        private extern static IntPtr Ocr(byte[] bts,int len,int width,int height,ref int outlen);

        [DllImport("ocr.dll", EntryPoint = "DeleteMemeoy", CallingConvention = CallingConvention.Cdecl)]
        private extern static void DeleteMemeoy(IntPtr ptr);

        #endregion

        public static int InitOcr()
        {
            return LoadConfig($@"{Environment.CurrentDirectory}\config.txt");
        }

        /// <summary>
        /// 图片识别
        /// </summary>
        /// <param name="bmp"></param>
        /// <returns></returns>
        public static List<OcrInfo> Ocr(Bitmap bmp)
        {
            var bts = bmp.ToByteArray();
            int outlen = 0;
            var ptr = Ocr(bts, bts.Length, bmp.Width, bmp.Height, ref outlen);
            var buffer = new byte[outlen];
            Marshal.Copy(ptr, buffer, 0, outlen);
            var json = System.Text.Encoding.UTF8.GetString(buffer, 0, buffer.Length);
            LogText.WriteLog(json);
            return json.ToObject<List<OcrInfo>>();
        }

        /// <summary>
        /// 指定区域识别
        /// </summary>
        /// <param name="bmp"></param>
        /// <param name="x1">左上角X坐标</param>
        /// <param name="y1">左上角Y坐标</param>
        /// <param name="x2">右下角X坐标</param>
        /// <param name="y2">右下角Y坐标</param>
        /// <returns></returns>
        public static List<OcrInfo> Ocr(Bitmap bmp,int x1,int y1,int x2,int y2)
        {
            var bts = Opencvsharp.CropBitmap(bmp, x1, y1, x2, y2).ToByteArray();
            int outlen = 0;
            var ptr = Ocr(bts, bts.Length, bmp.Width, bmp.Height, ref outlen);
            var buffer = new byte[outlen];
            Marshal.Copy(ptr, buffer, 0, outlen);
            var json = System.Text.Encoding.UTF8.GetString(buffer, 0, buffer.Length);
            LogText.WriteLog(json);
            var ocrinfos= json.ToObject<List<OcrInfo>>();
            foreach (var ocrinfo in ocrinfos)
            {
                ocrinfo.Text_Box_Position.LeftTop[0] += x1;
                ocrinfo.Text_Box_Position.LeftTop[1] += y1;
                ocrinfo.Text_Box_Position.RightTop[0] += x1;
                ocrinfo.Text_Box_Position.RightTop[1] += y1;
                ocrinfo.Text_Box_Position.LeftBottom[0] += x1;
                ocrinfo.Text_Box_Position.LeftBottom[1] += y1;
                ocrinfo.Text_Box_Position.RightBottom[0] += x1;
                ocrinfo.Text_Box_Position.RightBottom[1] += y1;
            }
            return ocrinfos;
        }
    }
}
