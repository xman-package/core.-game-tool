﻿using Core.GameTool.Framework.Common;
using Core.GameTool.Framework.Extensions;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Text;

namespace Core.GameTool.DnPlayer
{
    public class DnConsole
    {
        private const string Ldconsole = "ldconsole.exe";
        private const string Adb = "adb.exe";

        /// <summary>
        /// 启动模拟器
        /// </summary>
        /// <param name="index">模拟器ID</param>
        public static void Lanunch(int index)
        {
            var cmd = Dncmd.CreateLdCmd(index,Dncmd.Lanunch);
            ProcessHelper.ExecUtf8(cmd, Ldconsole);
        }

        /// <summary>
        /// 检测模拟器是否启动
        /// </summary>
        /// <param name="index">模拟器ID</param>
        /// <returns></returns>
        public static bool IsLanunch(int index)
        {
            var cmd = Dncmd.CreateLdadbCmd(index, Dncmd.IsLanunch);
            return ProcessHelper.ExecUtf8(cmd, Ldconsole).Trim().Equals("1");
        }

        /// <summary>
        /// 退出
        /// </summary>
        /// <param name="index">模拟器ID</param>
        public static void Quit(int index)
        {
            var cmd = Dncmd.CreateLdCmd(index, Dncmd.Quit);
            ProcessHelper.ExecUtf8(cmd, Ldconsole);
        }

        /// <summary>
        /// 获取模拟器列表
        /// </summary>
        /// <returns></returns>
        public static List<DnPlayer> GetDnList()
        {
            var dnplayers = new List<DnPlayer>();
            var lines = ProcessHelper.ExecUtf8(Dncmd.List2, Ldconsole);
            foreach (var line in lines.Split("\r\n"))
            {
                if (!line.IsNullOrEmpty())
                {
                    dnplayers.Add(new DnPlayer(line.Split(",")));
                }
            }
            return dnplayers;
        }

        /// <summary>
        /// 截图
        /// </summary>
        /// <param name="ipport">127.0.0.1:5555</param>
        /// <returns></returns>
        public static byte[] ScreenCap(string ipport)
        {
            var cmd = Dncmd.CreateAdbCmd(ipport, Dncmd.Screencap);
            var bts = ProcessHelper.ExecCommand(cmd, Adb);
            var hexstr = bts.ToHexString().Replace("0D-0A", "0A");
            return hexstr.ToBtArray16();
        }

        /// <summary>
        /// 打开APP
        /// </summary>
        /// <param name="ipport">127.0.0.1:5555</param>
        /// <param name="packagename">APP包名</param>
        public static void InvokeApp(string ipport,string packagename)
        {
            var cmd = Dncmd.CreateAdbCmd(ipport, $"{Dncmd.Invokeapp} {packagename}");
            ProcessHelper.ExecUtf8(cmd, Adb);
        }

        /// <summary>
        /// 点击操作
        /// </summary>
        /// <param name="ipport">127.0.0.1:5555</param>
        /// <param name="x">X坐标</param>
        /// <param name="y">Y坐标</param>
        /// <param name="delay">延迟，默认500毫秒</param>
        public static void Touch(string ipport, int x, int y,int delay=500)
        {
            var cmd = delay == 0 ? Dncmd.CreateAdbCmd(ipport, $"{Dncmd.Client} {x} {y}") : Dncmd.CreateAdbCmd(ipport, $"{Dncmd.Swipe} {x} {y} {x} {y} {delay}");
            ProcessHelper.ExecUtf8(cmd, Adb);
        }
        /// <summary>
        /// 滑动操作
        /// </summary>
        /// <param name="ipport">127.0.0.1:5555</param>
        /// <param name="x1">第一个点X坐标</param>
        /// <param name="y1">第一个点Y坐标</param>
        /// <param name="x2">第二个点X坐标</param>
        /// <param name="y2">第二个点Y坐标</param>
        /// <param name="delay"></param>
        public static void Swipe(string ipport, int x1, int y1, int x2, int y2, int delay=3000)
        {
            var cmd = Dncmd.CreateAdbCmd(ipport, $"{Dncmd.Swipe} {x1} {y1} {x2} {y2} {delay}");
            ProcessHelper.ExecUtf8(cmd, Adb);
        }
    }
}
