﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.GameTool.DnPlayer
{
    internal class Dncmd
    {
        public const string Invokeapp = " shell am start ";
        public const string Client = " shell input tap ";
        public const string Swipe = " shell input swipe ";
        public const string Screencap = " shell screencap -p ";
        public const string IsLanunch = " shell getprop sys.boot_completed ";
        public const string ClearApp = " shell pm clear ";
        public const string Dervices = " devices ";
        public const string Quit = " quit --index ";
        public const string Lanunch = " launch --index ";
        public const string List2 = " list2 ";

        /// <summary>
        /// 创建LdAdb命令
        /// </summary>
        /// <param name="index"></param>
        /// <param name="cmdstr"></param>
        /// <returns></returns>
        public static string CreateLdadbCmd(int index, string cmdstr)
        {
            return $"adb --index {index} --command \"{cmdstr}\"";
        }

        /// <summary>
        /// 创建Adb命令
        /// </summary>
        /// <param name="ipport">127.0.0.1:55557</param>
        /// <param name="cmdstr">命令</param>
        /// <returns></returns>
        public static string CreateAdbCmd(string ipport, string cmdstr)
        {
            return $"-s {ipport} {cmdstr} ";
        }

        /// <summary>
        /// 创建Ld命令
        /// </summary>
        /// <param name="index"></param>
        /// <param name="cmdstr"></param>
        /// <returns></returns>
        public static string CreateLdCmd(int index, string cmdstr)
        {
            return $"{cmdstr}{index}";
        }
    }
}
