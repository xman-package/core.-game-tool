﻿using Core.GameTool.Framework.Extensions;
using System;
using System.Collections.Generic;
using System.Text;

namespace Core.GameTool.DnPlayer
{
    public class DnPlayer
    {
        /// <summary>
        /// 模拟器序号
        /// </summary>
        public int Index { get; set; }
        /// <summary>
        /// 模拟器的IP和端口（127.0.0.1:55557）
        /// </summary>
        public string IP_Port => $"127.0.0.1:{5555 + Index * 2}";
        /// <summary>
        /// 模拟器名字
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 详细名字
        /// </summary>
        public string DetailName => $"{Index}_{Name}_{IP_Port}";
        /// <summary>
        /// 顶层窗口句柄
        /// </summary>
        public int Top_Win_Handler { get; set; }
        /// <summary>
        /// 绑定窗口句柄
        /// </summary>
        public int Bind_Win_Handler { get; set; }
        /// <summary>
        /// 是否进入android
        /// </summary>
        public bool Is_In_Android { get; set; }
        /// <summary>
        /// 进程PID
        /// </summary>
        public int Pid { get; set; }
        /// <summary>
        /// VBox进程PID
        /// </summary>
        public int Vbox_Pid { get; set; }

        public DnPlayer(string[]line)
        {
            this.Index = line[0].To<int>();
            this.Name = line[1];
            this.Top_Win_Handler = line[2].To<int>();
            this.Bind_Win_Handler = line[3].To<int>();
            this.Is_In_Android = line[4].To<bool>();
            this.Pid = line[5].To<int>();
            this.Vbox_Pid = line[6].To<int>();
        }
    }
}
