﻿using Core.GameTool.Framework.Extensions;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Text;

namespace Core.GameTool.Framework.Common
{
    public class ProcessHelper
    {
        public static Byte[] ExecCommand(string command, string name)
        {
            using Process process = new Process();
            process.StartInfo.FileName = name;
            process.StartInfo.Arguments = command;
            process.StartInfo.UseShellExecute = false;        //是否使用操作系统shell启动
            process.StartInfo.RedirectStandardInput = true;   //接受来自调用程序的输入信息
            process.StartInfo.RedirectStandardOutput = true;  //由调用程序获取输出信息
            process.StartInfo.RedirectStandardError = true;   //重定向标准错误输出
            process.StartInfo.CreateNoWindow = true;          //不显示程序窗口
            process.Start();
            FileStream baseStream = process.StandardOutput.BaseStream as FileStream;
            byte[] bts = null;
            int lastRead = 0;

            using (MemoryStream ms = new MemoryStream())
            {
                byte[] buffer = new byte[4096];
                do
                {
                    lastRead = baseStream.Read(buffer, 0, buffer.Length);
                    ms.Write(buffer, 0, lastRead);
                } while (lastRead > 0);

                bts = ms.ToArray();
            }
            return bts;
            //var hexstr = imageBytes.ToHexString().Replace("0D-0A", "0A");
            //return hexstr.ToByteArrayBy16();
            //using (MemoryStream ms = new MemoryStream(bts))
            //{
            //    Image img = Image.FromStream(ms);
            //    img.Save(@"c:\singleFrame.png", ImageFormat.Png);
            //}
            //return imageBytes;
        }

        public static string ExecUtf8(string command, string name)
        {
            var bts = ExecCommand(command, name);
            return System.Text.Encoding.UTF8.GetString(bts);
        }

    }
}
