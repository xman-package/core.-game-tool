﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Text;

namespace Core.GameTool.Framework.Common
{
    public static class ImgHelper
    {
        public static byte[] ToByteArray(this Bitmap bitmap)
        {
            using var ms = new MemoryStream();
            bitmap.Save(ms, System.Drawing.Imaging.ImageFormat.Bmp);
            var bts = ms.GetBuffer();
            return bts;
        }
        public static Bitmap ToBitmap(this byte[] bts)
        {
            using MemoryStream stream = new(bts);
            return new Bitmap(stream);
        }
    }
}
