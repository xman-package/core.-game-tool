﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Text;

namespace Core.GameTool.Framework.Extensions
{
    public static class ByteExtensions
    {
        public static string ToHexString(this byte[] bts)
        {
            return BitConverter.ToString(bts);
        }

        public static byte[] ToBtArray16(this string hexstr)
        {
            string[] strs = hexstr.Split('-');
            byte[] bts = new byte[strs.Length];
            for (int i = 0; i < strs.Length; i++)
            {
                bts[i] = byte.Parse(strs[i], System.Globalization.NumberStyles.AllowHexSpecifier);
            }
            return bts;
        }


    }
}
