﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.GameTool.Extensions
{
    public static class ByteExtensions
    {
        public static string ToHexString(this byte[] bts)
        {
            return BitConverter.ToString(bts);
        }

        public static byte[] ToByteArrayBy16(this string hexstr)
        {
            string[] strs = hexstr.Split('-');
            byte[] bts = new byte[strs.Length];
            for (int i = 0; i < strs.Length; i++)
            {
                bts[i] = byte.Parse(strs[i], System.Globalization.NumberStyles.AllowHexSpecifier);
            }
            return bts;
        }
    }
}
