﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Core.GameTool.Extensions
{
    public class LogText
    {
        private static readonly object locker = new object();

        public static void WriteLog(string information)
        {

            try
            {
                lock (locker)
                {
                    string logpath = System.AppDomain.CurrentDomain.SetupInformation.ApplicationBase;//获取当前执行程序的目录

                    if (!Directory.Exists(logpath + "\\log" + "\\" + DateTime.Now.Year.ToString() + "\\" + DateTime.Now.Month.ToString()))
                    {
                        logpath += "\\log" + "\\" + DateTime.Now.Year.ToString() + "\\" + DateTime.Now.Month.ToString();
                        Directory.CreateDirectory(logpath);
                    }
                    else
                    {
                        logpath += "\\log" + "\\" + DateTime.Now.Year.ToString() + "\\" + DateTime.Now.Month.ToString();
                    }

                    logpath += "\\" + DateTime.Now.Year.ToString() + "-" + DateTime.Now.Month.ToString() + "-" + DateTime.Now.Day.ToString() + ".txt";

                    if (!File.Exists(logpath))  //判断是否存在记录信息的文件
                    {
                        FileStream fs = File.Create(logpath);  //创建；
                        fs.Close();
                    }

                    StreamWriter sw = File.AppendText(logpath);
                    sw.WriteLine(DateTime.Now.ToString() + "\t" + information);

                    sw.Close();
                }
            }
            catch (Exception)
            {

            }
        }
    }
}
